class EmailWorker
  include Sidekiq::Worker

  def perform()
    @emails = Email.where(status: :not_sent)

    @emails.each do |email|
      EmailMailer.send_email(email).deliver
      email.update_attributes(status: :sent)
    end
  end
end