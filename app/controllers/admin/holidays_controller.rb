class Admin::HolidaysController < ApplicationController
  layout "admin"
  def index
    @holidays = Holiday.all
  end
  def show
    @holidays = Holiday.all
  end
  def new
    @holiday= Holiday.new
  end
  def create
    @holiday = Holiday.new(holiday_params)
    if @holiday.save
      redirect_to admin_holidays_path
    else
      render 'new'
    end
  end
  def destroy
    Holiday.find(params[:id]).destroy
    flash[:success] = 'Holiday Successfully deleted'
    redirect_to admin_holidays_path
  end
  def holiday_params
    params.require(:holiday).permit(:date, :reason)
  end
  def check_log_in
    if current_user.nil?
      redirect_to login_path
    end
  end
end
