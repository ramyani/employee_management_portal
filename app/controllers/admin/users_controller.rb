class Admin::UsersController < ApplicationController
  before_action :logged_in_user , only: [:show]
  layout "admin"
  def index
    @users = User.order(:id)
    @user_roles = UserRole.all
    @roles = Role.all
  end

  def show
    @user = User.find(params[:id])
    @user_roles = UserRole.all
    @roles = Role.all
    @manages = Manage.all
    @managers = Manage.select("user_id")
    @manager_ids = @managers.map do |manager| manager.user_id end
    @employees = Assignment.select("user_id")
    @employee_ids = @employees.map do |employee| employee.user_id end
    if @manager_ids.include?(@user.id)
       @assign_ids = Assignment.where(mng_id:@user).map do |assigns| assigns.user_id end
       @users = User.all
    else
      if @employee_ids.include?(@user.id)
        @assigned_manager_id = Assignment.find_by(user_id: @user).mng_id
        @manager_info = User.find(@assigned_manager_id)
      else
        @user
      end
    end
  end

  def add_manager
    Assignment.create(user_id: params[:id], mng_id: params[:user][:manages])
    flash[:messge] = 'Manager Assigned Sucessfully'
    redirect_to admin_index_url
  end

  def new
    @title = 'Sign Up'
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
     redirect_to users_path
    else
      render 'new'
    end

    @user.role = Role.find_by(role: 'Employee')
    @user.save
  end

  def edit
    @user_edit = User.find(params[:id])
    @manages = Manage.all
    @roles = Role.all
    @selected_user = @manages.map do |manage| manage.user_id end
    @assigns = Assignment.all.map do |assign| assign.user_id end
  end

  def update
    @user = User.find(params[:id])
    @path = request.headers["HTTP_REFERER"].to_s

    if params[:user][:manages] != nil
      Assignment.create(user_id: params[:id], mng_id: params[:user][:manages])
    end

    if @path.split("/").include?('edit')
      if @user.update(user_params)
        UserRole.where(user_id:params[:id]).update_all(role_id: params[:user][:role])
        flash[:notice] = 'Updated Sucessfully'
        redirect_to admin_users_path
      else
        flash[:notice] = 'Do Not Updated Sucessfully'
        render 'edit'
      end
    else
      redirect_to @path
    end
  end
  def destroy
  end

  private
   def user_params
      params.require(:user).permit(:name, :email, :mobile, :password, :password_confirmation)
   end

   def update_params
      params.require(:user).permit(:name, :email, :mobile)
   end
end
