class User < ApplicationRecord
  validates :name,  presence: true, length: { maximum: 500 }
  validates :email, presence: true, length: { maximum: 255 },uniqueness: { case_sensitive: false }
  validates :password, presence: true,length: { minimum: 8 }
  validates :mobile, presence: true, length: { is: 10 } ,numericality: { message: "%{value} is not a mobile number" }

  has_one :user_role
  has_one :role, :through => :user_role, dependent: :destroy
  has_many :leaveemps, dependent: :destroy

  validates :mobile, presence: true,length: { minimum: 10 ,maximum: 10 }
  enum role: { employee: '1', admin: '2', trainee: '3', intern: '4' }
  # has_one :roles, dependent: :destroy
  has_many :manages, dependent: :destroy
  has_many :assignments, dependent: :destroy
  has_many :leaveemps, dependent: :destroy
  has_many :admin
  has_secure_password

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def admin?
    if role.to_s == Role.find_by(role: 'Admin').id
      return true
    else
      return false
    end
  end
end