require 'rails_helper'

RSpec.describe UserRole, type: :model do
  scenario 'should belongs to user' do
    t = UserRole.reflect_on_association(:user)
    expect(t.macro).to eq(:belongs_to)
  end
  scenario 'should belongs to role' do
    t = UserRole.reflect_on_association(:user)
    expect(t.macro).to eq(:belongs_to)
  end
end
