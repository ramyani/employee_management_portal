require 'rails_helper'

RSpec.describe Leaveemp, type: :model do
    let(:leaveemps){Leaveemp.new(startdate: '10-03-2020', enddate: '11-03-2020') }
    context 'presence of startdate' do
      context 'if user does not have startdate ' do
        it 'should not be valid leave' do
          leaveemp = Leaveemp.new
          expect(leaveemp).to_not be_valid
        end
      end
    end
    context 'presence of startdate' do
      context 'if user does not have enddate ' do
        it 'should not be valid leave' do
          leaveemp = Leaveemp.new
          expect(leaveemp).to_not be_valid
        end
      end
    end
    context 'startdate grater than enddate' do
      it 'should not be valid leave' do
        leaveemp = Leaveemp.new(startdate: '13-03-2020', enddate: '11-03-2020')
        expect(leaveemp).to_not be_valid
      end
    end
end

