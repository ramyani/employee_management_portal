require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { User.new(name: 'Test User', email: 'test@test.com',mobile: '9999999999',password: '123456789' ,password_confirmation: '123456789') }
  let(:leaveemps){Leaveemp.new(startdate: '10-03-2020', enddate: '11-03-2020') }
  let(:roles){Role.new({ role: "Employee" }, { role: "Admin" }, { role: "Trainee"}, { role: "Intern"})}
  describe User, 'validation' do
    context 'presence of email' do
      context 'if user does not have email,name,mobile,password ' do
        it 'should not be valid user' do
          user = User.new
          expect(user).to_not be_valid
        end
      end
    end
    context 'minimum length of password' do
      it "password should have a minimum length" do
        user.password = user.password_confirmation = "a" * 7
        expect(user).to_not be_valid
      end
    end

    context 'minimum length of mobile number' do
      it "mobile number should have a minimum length of 10" do
        user.mobile = "9" * 7
        expect(user).to_not be_valid
      end
    end

    context 'length of mobile number' do
      it "mobile number should have a minimum length of 10" do
        user.mobile = "9" * 9
        expect(user).to_not be_valid
      end
    end

    context 'maximum length of phn number' do
      it "mobile number's length should not be grater than 10" do
        user.mobile = "9" * 12
        expect(user).to_not be_valid
      end
    end

    it "email should not be too long" do
        user.email = "a" * 266 + "@example.com"
        expect(user).to_not be_valid
    end

    it "name should not be too long" do
        user.name = "a" * 512
        expect(user).to_not be_valid
    end

    it "email addresses should be unique" do
      duplicate_user = user.dup
      user.save
      expect(duplicate_user).to_not be_valid
    end
  end
  describe User, 'association' do
    scenario 'should have' do
      t = User.reflect_on_association(:leaveemps)
      expect(t.macro).to eq(:has_many)
    end

    scenario 'should have' do
      t = User.reflect_on_association(:manages)
      expect(t.macro).to eq(:has_many)
    end

    scenario 'should has' do
      t = User.reflect_on_association(:role)
      expect(t.macro).to eq(:has_one)
    end

    scenario 'should has' do
      t = User.reflect_on_association(:user_role)
      expect(t.macro).to eq(:has_one)
    end
  end
end

