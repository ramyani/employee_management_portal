require 'rails_helper'

RSpec.feature "Admins", type: :feature do
  before :each do
    user = User.create(name: 'Test User', email: 'test@test.com',mobile: '9999999999',password: '123456789' ,password_confirmation: '123456789')
    roles = Role.create([{ role: "Employee" }, { role: "Admin" }, { role: "Trainee"}, { role: "Intern"}])
    holiday = Holiday.new
    user_role = UserRole.new
    visit "/admin"
    fill_in 'Email', with: 'test@test.com'
    fill_in 'Password', with: '123456789'
    click_button("Log In")
  end
  describe "Admin page should have" do
    scenario "all this links" do
     expect(page).to have_link ("Home")
     expect(page).to have_link ("Logout")
     expect(page).to have_link ("Leaves")
     expect(page).to have_link ("List of holidays")
     expect(page).to have_link ("Admin panel")
    end
  end
  describe "Admin panel link should " do
    scenario "have link " do
      click_link("Admin panel")
      expect(page).to have_current_path('/admin')
      expect(page).to have_link ("Users")
      expect(page).to have_link ("Holiday")
      click_link("Holiday")
      expect(page).to have_link ("Add")
      click_link("Add")
      expect(page).to have_field ("Date")
      expect(page).to have_field ("Reason")
      fill_in 'Date' , with: "2020/01/01"
      fill_in 'Reason', with: 'New Year'
      click_button("submit")
      expect(page).to have_current_path('/admin/holidays')
      click_link("Admin panel")
      click_link("Users")
      expect(page).to have_link ("Edit")
      #click_link("Edit")
      find(:xpath, "//tr[td[contains(.,'test@test.com')]]/td/a", :text => 'Edit').click
      expect(page).to have_field 'user[name]'
      fill_in 'user[name]', with: 'Test User'
      fill_in 'user[email]', with: 'test@test.com'
      fill_in 'user[mobile]', with: '9999999999'
      fill_in 'user[password]', with: '9999999999'
      fill_in 'user[password_confirmation]', with: '9999999999'
      expect(page).to have_selector("input", :count => 4)
      select 'Admin', from: 'user_role'
      expect(page).to have_button ("Update User")
      click_button("Update User")
      expect(page).to have_content("Updated Sucessfully")
      click_link ("Logout")
      expect(page).to have_current_path('/login')

    end
  end
end
