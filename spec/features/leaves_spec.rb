require 'rails_helper'

RSpec.feature "Leaves", type: :feature do
  before :each do
    user = User.create(name: 'Test User', email: 'test@test.com',mobile: '9999999999',password: '123456789' ,password_confirmation: '123456789')
    roles = Role.create([{ role: "Employee" }, { role: "Admin" }, { role: "Trainee"}, { role: "Intern"}])
    holiday = Holiday.new
    user_role = UserRole.new
    leaveemp = Leaveemp.new
    visit "/admin"
    fill_in 'Email', with: 'test@test.com'
    fill_in 'Password', with: '123456789'
    click_button("Log In")
  end
  describe "Leaves page should have apply link" do
    scenario "apply leave Successfully" do
      expect(page).to have_link ("Home")
      expect(page).to have_link ("Logout")
      expect(page).to have_link ("Leaves")
      click_link("Leaves")
      expect(page).to have_link ("Apply for Leave")
      expect(page).to have_link ("Upcoming Leaves")
      expect(page).to have_link ("Leave History")
      expect(page).to have_link ("All Leaves")
      click_link("Apply for Leave")
      expect(page).to have_field 'leaveemp[startdate]'
      expect(page).to have_field 'leaveemp[enddate]'
      fill_in 'leaveemp[startdate]', with: "2020/08/01"
      fill_in 'leaveemp[enddate]', with: "2020/08/10"
      click_button("Apply")
      expect(page).to have_content 'Successfully applied for leave'
       click_link("Leaves")
      click_link("Upcoming Leaves")
      expect(page).to have_content 'Upcoming Leaves'
    end
  end
  describe "Leaves page apply link" do
    scenario "should contain one error" do
      click_link("Leaves")
      click_link("Apply for Leave")
      fill_in 'leaveemp[startdate]', with: "2020/08/15"
      fill_in 'leaveemp[enddate]', with: "2020/08/10"
      click_button("Apply")
      expect(page).to have_content 'The form contains 1 error.'
      expect(page).to have_content "Leave start date and leave end date should be valid"
    end
    scenario "should contain two error" do
      click_link("Leaves")
      click_link("Apply for Leave")
      fill_in 'leaveemp[startdate]', with: ""
      fill_in 'leaveemp[enddate]', with: ""
      click_button("Apply")
      expect(page).to have_content 'The form contains 2 errors.'
      expect(page).to have_content "Startdate can't be blank"
      expect(page).to have_content "Enddate can't be blank"
    end
  end
  describe "upcoming link" do
    scenario "with no leaves" do
      click_link("Leaves")
      click_link("Upcoming Leaves")
      expect(page).to have_content 'You have no applied leaves'
    end
  end
   describe "Leave History" do
    scenario "should have" do
      click_link("Leaves")
      click_link("Leave History")
      expect(page).to have_content 'Your Previous Leaves'
    end
  end
end
