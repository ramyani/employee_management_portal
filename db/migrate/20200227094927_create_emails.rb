class CreateEmails < ActiveRecord::Migration[6.0]
  def change
    create_table :emails do |t|
      t.text :sender_email
      t.text :receiver_email
      t.text :subject
      t.text :body

      t.timestamps
    end
  end
end
